## taglist.vim

#### Description
The "Tag List" plugin is a source code browser plugin for Vim and
provides an overview of the structure of source code files and allows
you to efficiently browse through source code files for different
programming languages.  You can visit the taglist plugin home page for
more information:
	[http://vim-taglist.sourceforge.net](http://vim-taglist.sourceforge.net)

#### Acknowledge
This is only a mirror repository for taglist, in order that users of Vundle.vim can get the *latest* plugin easily.

#### Version
4.6

#### Usage
For Vundle.vim:
`Plugin 'injo/taglist.vim'`

